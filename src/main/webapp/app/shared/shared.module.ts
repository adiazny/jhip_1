import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Jhip1SharedLibsModule, Jhip1SharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [Jhip1SharedLibsModule, Jhip1SharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [Jhip1SharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Jhip1SharedModule {
  static forRoot() {
    return {
      ngModule: Jhip1SharedModule
    };
  }
}
