import { NgModule } from '@angular/core';

import { Jhip1SharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [Jhip1SharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [Jhip1SharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class Jhip1SharedCommonModule {}
