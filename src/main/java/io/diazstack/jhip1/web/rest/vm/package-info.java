/**
 * View Models used by Spring MVC REST controllers.
 */
package io.diazstack.jhip1.web.rest.vm;
